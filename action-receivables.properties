# -------------------------------------------------------------------------------------------
# NOTE : 
# 	This should be used only for DEV instance
# 	For Live instance, all variables should be defined as Environment variables.
# 	Environment variables hold precedence against what is defined in this file
# -------------------------------------------------------------------------------------------

# Allow local configuration to override Remote Externalized configuration
spring.cloud.config.allowOverride=true
# But, only System properties or Env variables (and not local config files) will override externalized configuration
spring.cloud.config.overrideSystemProperties=false



#############################################################################################
###### Global App Specific Configuration
#############################################################################################
# Logging level (DEBUG,INFO,WARN,FATAL)
logging.level.ROOT=INFO

# Used by Spring Boot (Application Context)
server.contextPath=/igtb-receivables

# Port 51002
server.port=51002

# Module Abbreviation Name (preferably <= 3 characters and only Upper Case Alphabets A-Z)
# e.g. LMs (for Liquidity), IPSH (for Payments), CNR (for Collections & Receivables)...
# This is used for deriving:
# 	- ElasticSearch Index name as <moduleAbbr>-requests (e.g. lms-requests, ipsh-requests, cnr-requests)
#	- Deriving Destination names in Release Trigger and State Update handler modules
ModuleAbbr=RCV

# Header key and value, to be used as security token while calling APIs for this module
iGTBD-AtomicAPI-SharedKey=I4qwGynNIt5DP5zUjjemHR1mEj8Ii6jq

# ES Database connection details
#ES.DB.Host=localhost
#ES.DB.Port=9200
#ES.DB.User=
#ES.DB.Password=

Redis.DB.Url=localhost:6379
Redis.DB.Password=

# To Enable/Disable DevMsg field in JSON response returned from API
# Y - enables DevMsg in output json response (may be useful for dev env)
# N - disables the same (recommended for production env) (this is default, if not specified)
EnableDevMsgInResponse=N

# Camel Messages to be logged at this level (Possible values: ERROR, WARN, INFO, DEBUG, TRACE, OFF)
CamelMessageLoggingLevel=DEBUG

# Release Batch Size
# It is max number of requests to be released per batch (Approved, Retry batch)
ReleaseBatchSize=200

# JMS Broker which is to be used by Commons for handling Release Batch Trigger requests, State Update requests
# Supported values (ActiveMQ / RabbitMQ) - default is ActiveMQ
# This is used for dynamically identifying destination names for subscribing to/publishing Release Batch or State Update events

INSTANCE.1.Http.ServiceUrl=http://localhost:51002

Backend.ValidateServiceUri=http://103.231.209.251:10004
#Backend.ValidateServiceUri=http://localhost:8081
Backend.ReleaseServiceUri=t3://103.231.209.251:10004

#############################################################################################
###### Purge Trigger Handler specific Configuration
#############################################################################################
# Name of the Apache Camel Component (which is configured at Domain level),
# which is to be used by Commons package for subscribing/publishing to Purge Batch Trigger events 
PurgeTriggerHandler.CamelComponent=rabbitmq
PurgeTriggerHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

# Minimum number of days the data must be retained in State Store (irrespective of what is specified in PurgeStateStoreTrigger)
PurgeTriggerHandler.MinRetentionDays=720
# Flag indicating whether purging of IN_PROGRESS requests from State Store is allowed or not (irrespective of what is specified in PurgeStateStoreTrigger)
PurgeTriggerHandler.PurgeOfInProgressAllowed=false


# Comma separated list of Payload Types, for which this module need to support running Purge Batches
PurgeTriggerHandler.PayloadTypes=Invoice

# Used as sleep time (in milliseconds) between each message retry, when msg is requeue'd.
# Keep it commented, unless need to override default value.
#MsgRetrySleepTime=250

# Used as max time (in seconds) a message to be retried for in requeue mode
# Keep it commented, unless need to override default value.
#MsgRetryMaxTime=86400

##################################################################
### Zipkin specific options
##################################################################
spring.zipkin.enabled=false
spring.sleuth.enabled=false
#spring.sleuth.web.additionalSkipPattern=.*/getDetails|.*/updateDetails
#spring.sleuth.sampler.probability=1.0
#spring.zipkin.baseUrl=http://localhost:9411



#############################################################################################
###### Release Retry Configuration
#############################################################################################
RelRetryCfgIdentifierKeys=INVOICE_ANY

###
# Release Retry Config Identifier key based configuration
# 
# RelRetryCfgIdentifier.<identifierKey>.payloadType=
# RelRetryCfgIdentifier.<identifierKey>.requestType=
# RelRetryCfgIdentifier.<identifierKey>.maxReleaseRetry=
# Where,
#	<payloadType,requestType> together forms a unique key combination for providing related configuration 
#	payloadType = type of the payload e.g. SweepStructure, LoanAgreement or any
#	requestType = type of the request e.g. create, update, delete or any
# 	maxReleaseRetry = to indicate how many times this message should be retried for releasing before it is marked as Failed
#			(field is optional - default value will be applied)
#
# Configuration is applied in following order of priority, for a combination of <payloadType> and <requestType>:
# 1. <payloadType> and <requestType> exactly match with values provided here.
# 2. Else - <payloadType> and "any"
# 3. Else - "any" and <requestType>
# 4. Else - "any" and "any"
# 5. Else - default retry configuration is applied
#
###
RelRetryCfgIdentifier.INVOICE_ANY.payloadType=Invoice
RelRetryCfgIdentifier.INVOICE_ANY.requestType=any
RelRetryCfgIdentifier.INVOICE_ANY.maxReleaseRetry=





##################################################################
### Release Connector Specific Configuration
##################################################################
RelConnector.JMS_CONNECTOR.route=direct:JmsConnectorRoute


### Transformer routes
# Invoice specific
RelConnector.INVOICE_TXFMR.route=direct:TransformInvoiceRoute


### Backend Endpoints
# Invoice specific
# Endpoint using JMS
#RelConnector.USER_JMS.endpoint=INSTANCE.1.AMQ.Component:topic:Release-UserQ?disableReplyTo=true
RelConnector.INVOICE_JMS.endpoint=INSTANCE.1.WLS.Component:queue:COM_INT_QUEUE?disableReplyTo=true
#RelConnector.INVOICE_JMS.endpoint=direct:ReleaseStub


### Routing Slips
# Invoice specific
RelConnector.InvoiceAny.routingSlip=${RelConnector.INVOICE_TXFMR.route},\
		${RelConnector.JMS_CONNECTOR.route},\
		${RelConnector.INVOICE_JMS.endpoint}

##################################################################
### Validation specific Connector routes
##################################################################
ValConnector.HTTP_CONNECTOR.route=direct:HttpConnectorRoute
ValConnector.HTTP_CONNECTOR.route.CREATE=direct:HttpConnectorRouteCreate
ValConnector.HTTP_CONNECTOR.route.UPDATE=direct:HttpConnectorRouteUpdate
ValConnector.HTTP_CONNECTOR.route.DELETE=direct:HttpConnectorRouteDelete

### Transformer routes
# Invoice specific
#ValConnector.Invoice_TXFMR.route=direct:TransformValidateUserRoute


### Backend Endpoints
# Invoice specific
# Validation Endpoint using Interface approach
#ValConnector.USER_HTTP.endpoint=jetty://http://localhost:51003/stubs/validators/v1/user
ValConnector.Invoice_HTTP.endpoint=jetty://${Backend.ValidateServiceUri}/cmsweb/igtb-cnr/v1/corporate/invoices
#ValConnector.Invoice_HTTP.endpoint=direct:ValidateStub



### Routing Slips
# Invoice specific
ValConnector.InvoiceCreate.routingSlip= ${ValConnector.HTTP_CONNECTOR.route.CREATE},\
${ValConnector.Invoice_HTTP.endpoint}
ValConnector.InvoiceUpdate.routingSlip= ${ValConnector.HTTP_CONNECTOR.route.UPDATE},\
${ValConnector.Invoice_HTTP.endpoint}
ValConnector.InvoiceDelete.routingSlip= ${ValConnector.HTTP_CONNECTOR.route.DELETE},\
${ValConnector.Invoice_HTTP.endpoint}



##################################################################
### Backend Instance specific configuration for Camel Component
##################################################################
# This is used in Spring Camel beans XML

# Represents DataCenter region, country the process events/data belongs to
DataCenter.Region=
DataCenter.Country=

# Represents service key regex pattern (product/subproduct), this module is supposed to process events/data related to
Digital.ServiceKey.Patterns=colrec/rec.*


#Digital.JmsBroker.Host=localhost
#Digital.JmsBroker.Port=61001
#Digital.JmsBroker.User=guest
#Digital.JmsBroker.Password=guest
#Digital.JmsBroker.VHost=/
#Digital.JmsBroker.Type=RabbitMQ


# Message Broker which is to be used by Commons for Release Batch Triggers, State Update Events and Event Publisher
# Supported value - RabbitMQ
Digital.MsgBroker.Type=RabbitMQ
# This is used by Commons for connecting to RabbitMQ Server
Digital.RabbitMQ.Host=localhost
Digital.RabbitMQ.Port=61001
Digital.RabbitMQ.User=guest
Digital.RabbitMQ.Password=guest
Digital.RabbitMQ.VHost=/


# Name of the Apache Camel Component,
# which is to be used by Commons package for publishing Events / Triggers
MsgPubHandler.CamelComponent=rabbitmq



#############################################################################################
###### Release Trigger Handler specific Configuration
#############################################################################################
# Name of the Apache Camel Component,
# which is to be used by Commons package for subscribing/publishing to Release Batch Trigger events 
RelTriggerHandler.CamelComponent=rabbitmq
RelTriggerHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

# Comma separated list of Payload Types, for which this module need to support running Release Batches
RelTriggerHandler.PayloadTypes=Invoice



#################################################################################################
###### State Update Handler specific Configuration
#################################################################################################
# Name of the Apache Camel Component,
# which is to be used by Commons package for subscribing/publishing to B/E State Update events 
StateUpdHandler.CamelComponent=rabbitmq
StateUpdHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30


#BACKEND WEBLOGIC Initial Connection factory , provider url and JNDI connection factory name settings
INSTANCE.1.WLS.InitialContextFactory=weblogic.jndi.WLInitialContextFactory
#INSTANCE.1.WLS.ProviderUrl=t3://10.10.7.143:20013
INSTANCE.1.WLS.ProviderUrl=${Backend.ReleaseServiceUri}
INSTANCE.1.WLS.JndiConnFactoryName=COM_INT_QCF

StateUpdHandler.wlsCamelComponent=INSTANCE.1.WLS.Component
RelConnector.INVOICE_JMS.outqueue_endpoint=INVOICE_RESP_QUEUE


